/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

/**
 *
 * @author shanmat20
 */
public class Collision
{

    public static CollisionResult boxTest(Vector2 pos, Vector2 limit, Vector2 size)
    {
        CollisionResult res = new CollisionResult();
        if (pos.x + size.x > limit.x || pos.x - size.x < 0)
        {
            res.collideX = true;
            if (pos.x - size.x < 0)
            {
                res.pen.x = size.x - pos.x;
            } else
            {
                res.pen.x = limit.x - size.x - pos.x;
            }
        }
        if (pos.y + size.y > limit.y || pos.y - size.y < 0)
        {
            res.collideY = true;
            if (pos.y - size.y < 0)
            {
                res.pen.y = size.y - pos.y;
            } else
            {
                res.pen.y = limit.y - size.y - pos.y;
            }
        }
        return res;
    }

    public static CollisionResult circleTest(Vector2 a, float ra, Vector2 b, float rb)
    {
        CollisionResult res = new CollisionResult();
        float r = ra + rb;
        Vector2 d = b.copy().sub(a);
        float dist = d.dist2();
        if (dist <= r * r)
        {
            res.collideX = true;
            res.norm = d.normalize();
            res.pen.x = (float) Math.max(0, r - Math.sqrt(dist));
        }
        return res;
    }
}
