/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

/**
 *
 * @author shanmat20
 */
public class Vector2
{
    public float x;
    public float y;
    
    public Vector2()
    {
        this.x = 0;
        this.y = 0;
    }
    
    public Vector2(float s)
    {
        this.x = s;
        this.y = s;
    }
    
    public Vector2(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    
    public Vector2 copy()
    {
        return new Vector2(this.x, this.y);
    }
    
    public Vector2 set(Vector2 v)
    {
        this.x = v.x;
        this.y = v.y;
        return this;
    }
    
    public Vector2 add(Vector2 v)
    {
        this.x += v.x;
        this.y += v.y;
        return this;
    }
    
    public Vector2 add(float x, float y)
    {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector2 sub(Vector2 v)
    {
        this.x -= v.x;
        this.y -= v.y;
        return this;
    }
    
    public Vector2 scale(float s)
    {
        this.x *= s;
        this.y *= s;
        return this;
    }

    public float dot(Vector2 v)
    {
        return this.x * v.x + this.y * v.y;
    }

    public float dist2()
    {
        return this.x * this.x + this.y * this.y;
    }

    public Vector2 normalize()
    {
        float inv = 1 / (float) Math.sqrt(this.dist2());
        this.x *= inv;
        this.y *= inv;
        return this;
    }
}
