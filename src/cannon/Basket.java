package cannon;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Basket
{
    private Vector2 pos;
    private float r;
    private static final float WIDTH = 50;

    public Basket()
    {
        pos = new Vector2(400, 400);
        r = 20.0f;
    }

    public void collide(Ball b)
    {
        Vector2[] positions = {new Vector2(pos.x - WIDTH , pos.y),  new Vector2(pos.x + WIDTH, pos.y)};

        for (int i = 0; i < 2; i++)
        {
            CollisionResult res = Collision.circleTest(b.getPos(), b.getRadius(), positions[i], r);
            if (res.collideX)
            {
                // r = d - 2(d dot n)n
                Vector2 n = res.norm.copy();
                Vector2 oldV = b.getVel();
                Vector2 v = oldV.sub(n.scale(2 * n.dot(oldV)));

                Vector2 p = b.getPos().add(res.norm.scale(-res.pen.x));
//                System.out.println(res.pen.x);

//            Vector2 p = b.getPos();

                b.update(p, v);
            }
        }


    }

    public void draw(GraphicsContext g)
    {
        g.setFill(Color.BLACK);
        g.fillOval(pos.x - WIDTH - r, pos.y - r, 2 * r, 2 * r);
        g.fillOval(pos.x + WIDTH - r, pos.y - r, 2 * r, 2 * r);
    }
}
