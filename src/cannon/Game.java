/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;

/**
 *
 * @author shanmat20 9-20-18
 */
public class Game extends Application
{

    private static final int WIDTH = 800;
    private static final int HEIGHT = 800;

    Canvas canvas;
    long lastTime;
    Input input;

    Cannon cannon;
    ArrayList<Ball> balls;
    Basket basket;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        input = new Input();
        primaryStage.setTitle("Cannon");
        Group root = new Group();
        canvas = new Canvas(WIDTH, HEIGHT);
        Scene scene = new Scene(root);
        GraphicsContext g = canvas.getGraphicsContext2D();
        root.getChildren().add(canvas);

        input.register(scene);

        final long startNanoTime = System.nanoTime();

        balls = new ArrayList<>();
        cannon = new Cannon();
        basket = new Basket();

        AnimationTimer timer;
        timer = new AnimationTimer()
        {
            @Override
            public void handle(long now)
            {
                double t = (now - lastTime) / 1000000000.0;

                cannon.input(input);

                if (input.mousePressed() && t > 0.15)
                {
                    lastTime = now;
                    balls.add(cannon.spawnBall(input.getMousePos()));
                }

                if (input.keyPressed(KeyCode.R))
                {
                    balls.clear();
                }

                cannon.update();
                update();

                render(g);
            }
        };
        timer.start();

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void render(GraphicsContext g)
    {
        g.setFill(Color.WHITE);
        g.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        for (Ball b : balls)
        {
            float r = b.getRadius();
            g.setFill(b.getColor());
            g.fillOval(b.getPos().x - r, b.getPos().y - r, 2 * r, 2 * r);
        }

        cannon.draw(g);
        basket.draw(g);

    }

    private void update()
    {
        for (Ball b : balls)
        {
            Vector2 p = b.getPos();
            Vector2 v = b.getVel();
            b.update(p.add(v), v.add(0, 0.3f));
            float r = b.getRadius();
            CollisionResult res = Collision.boxTest(p, new Vector2(WIDTH, HEIGHT), new Vector2(r));
            if (res.collideX)
            {
                v.x *= -0.65;
            }
            if (res.collideY)
            {
                v.y *= -0.65;
                if (Math.abs(v.y) < 1.4)
                {
                    v.y = 0;
                    v.x *= 0.97;
                }
            }
            p.add(res.pen);

            basket.collide(b);
        }
    }
}
