/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

import javafx.scene.paint.Color;

/**
 *
 * @author shanmat20
 */
public class Ball
{
    private Vector2 pos;
    private Vector2 vel;
    private Color color;
    private float radius;
    
    public Ball(Vector2 pos, Vector2 vel, Color color, float r)
    {
        this.pos = pos.copy();
        this.vel = vel;
        this.color = color;
        this.radius = r;
    }
    
    public Vector2 getPos()
    {
        return pos;
    }
    
    public Vector2 getVel()
    {
        return vel;
    }
    
    public Color getColor()
    {
        return color;
    }
    
    public float getRadius()
    {
        return radius;
    }
    
    public void update(Vector2 pos, Vector2 vel)
    {
        this.pos.set(pos);
        this.vel.set(vel);
    }
}
