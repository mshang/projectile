/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

/**
 *
 * @author shanmat20
 */
public class CollisionResult
{
    public Vector2 pen;
    public Vector2 norm;
    public boolean collideX;
    public boolean collideY;
    
    public CollisionResult()
    {
        pen = new Vector2();
        norm = new Vector2();
    }
}
