/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

import javafx.scene.input.KeyCode;
import javafx.scene.Scene;

import java.util.HashMap;

/**
 *
 * @author shanmat20
 */
public class Input
{

    private HashMap<KeyCode, Boolean> keys;
    private Vector2 mousePos;
    private boolean mousePressed;

    public Input()
    {
        keys = new HashMap<>();
        for (KeyCode key : KeyCode.values())
        {
            keys.put(key, false);
        }

        mousePos = new Vector2();
        mousePressed = false;
    }

    public void register(Scene scene)
    {
        scene.setOnKeyPressed(event ->
        {
//            System.out.println("Key pressed: " + event.getCode().toString());
            keys.put(event.getCode(), true);
        });

        scene.setOnKeyReleased(event ->
        {
//            System.out.println("Key released: " + event.getCode().toString());
            keys.put(event.getCode(), false);
        });

        scene.setOnMouseMoved(event ->
        {
            mousePos.x = (float) event.getSceneX();
            mousePos.y = (float) event.getSceneY();
        });
        
        scene.setOnMouseDragged(event ->
        {
            mousePos.x = (float) event.getSceneX();
            mousePos.y = (float) event.getSceneY();
        });
        
        scene.setOnMousePressed(event -> {
//            System.out.println("Mouse pressed");
            mousePressed = true;
        });
        
        scene.setOnMouseReleased(event -> {
            mousePressed = false;
        });
    }

    public boolean keyPressed(KeyCode key)
    {
        return keys.get(key);
    }
    
    public Vector2 getMousePos()
    {
        return mousePos;
    }
    
    public boolean mousePressed()
    {
        return mousePressed;
    }
}
