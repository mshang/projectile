/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cannon;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;

/**
 *
 * @author shanmat20
 */
public class Cannon
{

    private Vector2 pos;
    private Vector2 vel;
    private float angle;

    public Cannon()
    {
        pos = new Vector2(50, 750);
        vel = new Vector2();
    }

    public void input(Input in)
    {
        Vector2 mousePos = in.getMousePos();
        angle = (float) Math.atan2(mousePos.y - pos.y, mousePos.x - pos.x);

        final float accel = 1.5f;

        if (in.keyPressed(KeyCode.W))
        {
            vel.y -= accel;
        }
        if (in.keyPressed(KeyCode.S))
        {
            vel.y += accel;
        }
        if (in.keyPressed(KeyCode.A))
        {
            vel.x -= accel;
        }
        if (in.keyPressed(KeyCode.D))
        {
            vel.x += accel;
        }
    }

    public void update()
    {
        pos.add(vel);
        vel.scale(0.9f);

        final float s = 20;
        
        CollisionResult res = Collision.boxTest(pos, new Vector2(800, 800), new Vector2(s));
        pos.add(res.pen);
        if (res.collideX)
        {
            vel.x *= -0.65;
        }
        if (res.collideY)
        {
            vel.y *= -0.65;
        }
    }

    public void draw(GraphicsContext g)
    {
        g.save();

        g.setFill(Color.ORANGE);

        Rotate rot = new Rotate(angle * 180 / Math.PI, pos.x, pos.y);
        g.transform(new Affine(rot));
        g.fillRect(pos.x - 25, pos.y - 15, 50, 30);
        g.setFill(Color.RED);
        g.fillRect(pos.x + 15, pos.y - 15, 10, 30);

        g.restore();
    }

    public Ball spawnBall(Vector2 mousePos)
    {
        float pow = (float) Math.hypot(pos.y - mousePos.y, pos.x - mousePos.x);
        pow = pow / 45 + 4;

        Vector2 ballVel = new Vector2((float) Math.cos(angle), (float) Math.sin(angle));
        ballVel.scale(pow).add(vel.copy().scale(0.5f));
        Color color = new Color(Math.random(), Math.random(), Math.random(), 1.0);
        float r = (float) (10 + 10 * Math.random());
        return new Ball(pos, ballVel, color, r);
    }

    public Vector2 getPosition()
    {
        return pos;
    }

    public Vector2 getVel()
    {
        return vel;
    }

    public float getAngle()
    {
        return angle;
    }
}
